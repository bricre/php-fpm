PHP-FPM container with various add-ons for Britannica Creations use

Inspired by  [hermsi/alpine-fpm-php](https://github.com/Hermsi1337/docker-fpm-php)

Add-ons (Other than default php add-ons):
* intl
* bcmath
* xsl
* zip
* soap
* pdo_mysql
* gd
* mongodb
* gearman
* pcntl
* rar


DataDog APM Tracing: https://docs.datadoghq.com/tracing/languages/php/


update image: 2021-10-01